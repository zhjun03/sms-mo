<?php
namespace Smsmo\DevMo;
/**
 * Author:Robert
 *
 */
class Mo
{
    public $sdkUrl;
    public $sn;
    public $pwd;
    public $error;
    public $timeout = 30;
    public $returnData;

    //推送数据解析
    public function setSdkData($data)
    {
        if (!$data) {
            $this->error = '上行推送方式数据不能为空';
            return false;
        }
        $arrData = explode(';', $data);
        $arrData = array_filter($arrData);
        if (empty($arrData)) {
            $this->error = '上行推送方式数据无法数组化';
            return false;
        }
        foreach ($arrData as $k => &$v) {
            $v = explode(',', $v);
            foreach ($v as $k1 => &$v1) {
                $v1 = trim($v1, ' ');
                if ($k1 == 3) {
                    $v1 = iconv('gb2312', 'UTF-8', urldecode($v1));
                }
            }
        }
        $this->returnData = json_encode($arrData);
        return $this->returnData;
    }
    public function setSdkUrl($url)
    {
        if (!$this->sn) {
            $this->error = '软件序列号sn未设置';
            return false;
        }
        if (!$this->pwd) {
            $this->error = '软件序列号密码未设置';
            return false;
        }
        //http://sdk.entinfo.cn:8060/webservice.asmx/mo?sn=$SN&pwd=$PWD
        $mdPwd = strtoupper(md5($this->sn.$this->pwd));
        $sdkUrl = $url.'/webservice.asmx/mo?sn='.$this->sn.'&pwd='.$mdPwd;
        $this->sdkUrl = $sdkUrl;
        return $sdkUrl;
    }
    public function setSn($sn)
    {
        $this->sn = $sn;
    }
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
    }
    //主动请求获取
    public function getUrlData()
    {
        if (!$this->sdkUrl) {
            $this->error = 'sdkUrl地址未设置';
            return false;
        }
        $curlData = $this->getCurl($this->sdkUrl);
        $arrOne = explode(PHP_EOL, $curlData);
        $arrTwo = [];
        if (!empty($arrOne)) {
            foreach ($arrOne as $k => $v) {
                $v = preg_replace("/\s*\<[\s\S]*?\>\s*/", '', $v);
                if ($v) {
                    $vArr = explode(',', $v);
                    $vArr1 = [];
                    if (!empty($vArr)) {
                        if (sizeof($vArr) == 1) {
                            $this->error = '上行主动获取错误,返回错误代码'.$vArr[0];
                            return false;
                        }
                        foreach ($vArr as $k1 => $v1) {
                            $v1 = trim($v1, ' ');
                            if ($v1) {
                                if ($k1 == 3) {
                                    $v1 = iconv('gb2312', 'UTF-8', urldecode($v1));
                                }
                                $vArr1[] = $v1;
                            }
                        }
                    }
                    if (!empty($vArr1)) {
                        $arrTwo[] = $vArr1;
                    }
                }
            }
        }
        $this->returnData = json_encode($arrTwo);
        return $this->returnData;
    }

    private function getCurl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        $status = curl_getinfo($curl);
        if ($status['http_code'] != 200) {
            $this->error = '上行主动获取请求错误,错误信息:'.$content;
            return false;
        }
        curl_close($curl);
        return $content;
    }


}